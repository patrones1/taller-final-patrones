package pizzeria;

public class Pizza {

    private Boolean conExtraQueso;
    private Boolean conPepino;
    private Boolean conJamon;
    private Boolean conPinna;
    private Boolean conBocadillo;
    private String descripcionPizza;

    public Pizza() {

        this.conExtraQueso = false;
        this.conPepino = false;
        this.conJamon = false;
        this.conPinna = false;
        this.conBocadillo = false;
        this.descripcionPizza = "Pizza con base de queso";

    }

    public void agregarExtraQueso() {

        this.conExtraQueso = true;

    }

    public void agregarPepino() {

        this.conPepino = true;

    }

    public void agregarJamon() {

        this.conJamon = true;

    }

    public void agregarPinna() {

        this.conPinna = true;

    }

    public void agregarBocadillo() {

        this.conBocadillo = true;

    }

    public void darDescripcionPizza() {

        if (this.conExtraQueso) {

            this.descripcionPizza += " - con extra queso";

        }

        if (this.conPepino) {

            this.descripcionPizza += " - con pepino";

        }

        if (this.conJamon) {

            this.descripcionPizza += " - con jamon";

        }

        if (this.conPinna) {

            this.descripcionPizza += " - con pinna";

        }

        if (this.conBocadillo) {

            this.descripcionPizza += " - con bocadillo";

        }

        System.out.println(this.descripcionPizza);

    }

}
