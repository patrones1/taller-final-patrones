package pizzeria;

public class Main {
    public static void main(String[] args) {

        Cliente cliente001 = new Cliente("Juan");

        Pizza pizza001 = cliente001.pedirPizza();

        pizza001.agregarExtraQueso();
        pizza001.agregarPepino();
        pizza001.agregarJamon();
        pizza001.agregarPinna();
        pizza001.agregarBocadillo();

        pizza001.darDescripcionPizza();

        Pizza pizza002 = cliente001.pedirPizza();

        pizza002.darDescripcionPizza();

        Cliente cliente002 = new Cliente("Maria");

        Pizza pizza003 = cliente002.pedirPizza();

        pizza003.agregarPinna();

        pizza003.darDescripcionPizza();

        Pizza pizza004 = cliente002.pedirPizza();

        pizza004.agregarBocadillo();
        pizza004.agregarExtraQueso();

        pizza004.darDescripcionPizza();

        Pizza pizza005 = cliente002.pedirPizza();

        pizza005.agregarPepino();
        pizza005.agregarJamon();
        pizza005.agregarPinna();

        pizza005.darDescripcionPizza();

        cliente001.darPuntosAcumulados();

        cliente002.darPuntosAcumulados();

    }

}
