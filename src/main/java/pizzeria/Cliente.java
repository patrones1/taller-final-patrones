package pizzeria;

public class Cliente {

    private final String nombreCliente;
    private int puntosCliente;

    public Cliente(String nombreCliente) {

        this.nombreCliente = nombreCliente;
        this.puntosCliente = 0;

    }

    public Pizza pedirPizza() {

        Pizza pizza = new Pizza();

        this.acumularPuntos();

        return pizza;

    }

    private void acumularPuntos() {

        this.puntosCliente += 100;

        System.out.println("Gracias por su compra, " + this.nombreCliente + " has ganado 100 puntos por la compra, para un total acumulado de " + this.puntosCliente + " puntos.");

    }

    public void darPuntosAcumulados() {

        System.out.println(this.nombreCliente + ", has acumulado un total de " + this.puntosCliente + " puntos.");

    }

}
