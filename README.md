**Taller Final Patrones**

El sistema a desarrollar consiste en una pizzería donde hay una pizza base sencilla de queso, a este se le pueden agregar más ingredientes es diferentes ordenes según la receta, sus ungrwdientes pueden ser queso adicional, pepino, jamón, piña y bocadillo.

así mismo existe un plan de fidelización donde cae compra permite la acumulación de puntos a cada cliente, dichos puntos se acumulan sin importar la sede o la pide solicitada.

la idea es solucionar el sistema a nivel de diseño y codificación haciendo uso de dos patrones, la entrega es vía gitlab justificando el motivo de usar los dos patrones, el proyecto debe compilar y estar completo. 

**Patrones aplicados:**

- Builder > Decorator

- Observer > Singleton
